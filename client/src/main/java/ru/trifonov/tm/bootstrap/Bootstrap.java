package ru.trifonov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;
import java.util.*;

@Setter
@Getter
public final class Bootstrap {
    @NotNull private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.trifonov.tm.terminal").getSubTypesOf(AbstractCommand.class);
    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();
    @NotNull private final TerminalService terminalService = new TerminalService();
    @Nullable private Session currentSession;

    public void start() {
        System.out.println("*** WELCOME TO TASK MANAGER *** \n Enter terminal \"help\" for watch all commands");
        try {
            init(classes);
            while (true) {
                System.out.println("\n Enter command:");
                @NotNull final AbstractCommand abstractCommand =
                        terminalService.getCommands().get(terminalService.getInCommand());
                if (abstractCommand == null) {
                    System.out.println("There is no such command");
                    continue;
                }
                execute(abstractCommand);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void init(@NotNull final Set<Class<? extends AbstractCommand>> commandsClass
    ) throws InstantiationException, IllegalAccessException {
        for (@NotNull final Class clazz : commandsClass) {
            registry(clazz);
        }
    }

    private void registry(@NotNull final Class clazz
    ) throws IllegalAccessException, InstantiationException {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        @NotNull final String nameCommand = abstractCommand.getName();
        @NotNull final String descriptionCommand = abstractCommand.getDescription();
        terminalService.getCommands().put(nameCommand, abstractCommand);
    }

    private void execute (@Nullable final AbstractCommand abstractCommand
    ) {
        try {
            abstractCommand.execute();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}


