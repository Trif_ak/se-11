package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserRemoveAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL USERS]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getUserEndpoint().deleteAllUser(currentSession);
        System.out.println("[OK]");
    }
}
