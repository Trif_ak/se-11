package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {
    @NotNull
    private final String PROPERTY = "/application.properties";
    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() throws IOException {
        InputStream inputStream = Properties.class.getResourceAsStream(PROPERTY);
        properties.load(inputStream);
    }

    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @Nullable final String envHost = System.getProperty("server.host");
        if (envHost == null) return propertyHost;
        return envHost;
    }

    @Override
    public String getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port");
        @Nullable final String envPort = System.getProperty("server.port");
        if (envPort == null) return propertyPort;
        return envPort;
    }

    @Override
    public String getServerSalt() {
        return properties.getProperty("server.salt");
    }

    @Override
    public Integer getServerCycle() {
        return Integer.parseInt(properties.getProperty("server.cycle"));
    }
}
