package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;
import ru.trifonov.tm.util.IdUtil;

import java.util.List;

public final class UserService extends ComparatorService implements IUserService {
    @NotNull private IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) throw new NullPointerException("Enter correct data");
        userRepository.persist(user);
    }

    @Override
    public User existsUser(
            @Nullable final String login, @Nullable final String password
    ) throws NullPointerException {
        if (login == null || login.trim().isEmpty()) return null;
        if (password == null || password.trim().isEmpty()) return null;
        return userRepository.existsUser(login, HashUtil.md5(password));
    }

    @Override
    public void registrationUser(
            @Nullable final String login, @Nullable final String password
    ) throws NullPointerException {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!userRepository.getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.REGULAR_USER);
        userRepository.persist(user);
    }

    @Override
    public void registrationAdmin(
            @Nullable final String login, @Nullable final String password
    ) throws NullPointerException {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!userRepository.getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.persist(user);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String password, @Nullable final RoleType roleType
    ) throws NullPointerException {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.merge(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User get(@Nullable final String id) throws NullPointerException {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.get(id);
    }

    @Override
    public void delete(@Nullable final String id) throws NullPointerException {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        userRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<User> users
    ) {
        if (users == null) return;
        for (@NotNull final User user : users) {
            userRepository.persist(user);
        }
    }

    @Override
    public void addUser() {
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordMD5(HashUtil.md5("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userRepository.persist(admin);

        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordMD5(HashUtil.md5("user"));
        user.setRoleType(RoleType.REGULAR_USER);
        userRepository.persist(user);
    }

    @Override
    public void changePassword(
            @Nullable final String userId, @Nullable final String newPassword
    ) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (newPassword == null || newPassword.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final String newPasswordMD5 = HashUtil.md5(newPassword);
        userRepository.changePassword(userId, newPasswordMD5);
    }
}