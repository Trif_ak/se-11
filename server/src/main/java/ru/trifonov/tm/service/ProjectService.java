package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.api.service.IProjectService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.util.IdUtil;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends ComparatorService implements IProjectService {
    @NotNull private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void persist(@Nullable final Project project) throws NullPointerException {
        if (project == null) throw new NullPointerException("Enter correct data");
        projectRepository.persist(project);
    }
    @Override
    public void insert(
            @Nullable final String name, @Nullable final String userId,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws NullPointerException, ParseException {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.persist(project);
    }

    @Override
    public void update(
            @Nullable final String name, @Nullable final String id,
            @Nullable final String userId, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) throws ParseException, NullPointerException {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, id, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.merge(project);
    }

    @Override
    public Project get(@Nullable final String id, @Nullable final String userId) throws NullPointerException {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.get(id, userId);
    }

    @Override
    public List<Project> getAllOfUser(@Nullable final String userId) throws NullPointerException {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.getAll(userId);
    }

    @Override
    public void delete(
            @Nullable final String id, @Nullable final String userId
    ) throws NullPointerException {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.delete(id, userId);
    }

    @Override
    public void deleteAll(@Nullable final String userId) throws NullPointerException {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.deleteAll(userId);
    }

    @Override
    public List<Project> sortBy(
            @Nullable final String userId, @Nullable final String comparatorName
    ) throws NullPointerException {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Project> projects = getAllOfUser(userId);
        if (projects == null || projects.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> getByPartString(
            @Nullable final String userId, @Nullable final String partString
    ) throws NullPointerException {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.getByPartString(userId, partString);
    }

    @Override
    public List<Project> getAll() {
        return projectRepository.getAll();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            projectRepository.persist(project);
        }
    }
}