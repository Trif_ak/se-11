package ru.trifonov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.api.service.IDomainService;
import ru.trifonov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

public final class DomainService extends AbstractService implements IDomainService {
    @NotNull final ServiceLocator serviceLocator;

    public DomainService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(serviceLocator.getProjectService().getAll());
        domain.setTasks(serviceLocator.getTaskService().getAll());
        domain.setUsers(serviceLocator.getUserService().getAll());
        domain.setSessions(serviceLocator.getSessionService().getAll());
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getSessionService().load(domain.getSessions());
        serviceLocator.getUserService().load(domain.getUsers());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
    }

    @Override
    public void domainSerializable() throws IOException {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try (@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domain");
             @NotNull final ObjectOutputStream domainObjectOut = new ObjectOutputStream(domainOut)
        ) {
            domainObjectOut.writeObject(domain);
        }
    }

    @Override
    public void domainDeserializable() throws IOException, ClassNotFoundException {
        try (@NotNull final FileInputStream domainInput = new FileInputStream("server/src/main/files/domain");
             @NotNull final ObjectInputStream domainObjectInput = new ObjectInputStream(domainInput)
        ) {
            load((Domain) domainObjectInput.readObject());
        }
    }

    @Override
    public void domainSaveJaxbXML() throws JAXBException {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller domainMarshaller = domainContext.createMarshaller();
        domainMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        domainMarshaller.marshal(domain, new File("server/src/main/files/domainJaxb.xml"));
    }

    @Override
    public void domainLoadJaxbXML() throws JAXBException {
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller domainUnmarshal = domainContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) domainUnmarshal.unmarshal(new File("server/src/main/files/domainJaxb.xml"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }

    @Override
    public void domainSaveJaxbJSON() throws JAXBException {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller domainMarshaller = domainContext.createMarshaller();
        domainMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        domainMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        domainMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        domainMarshaller.marshal(domain, new File("server/src/main/files/domainJaxb.json"));
    }

    @Override
    public void domainLoadJaxbJSON() throws JAXBException {
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller domainUnmarshal = domainContext.createUnmarshaller();
        domainUnmarshal.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        domainUnmarshal.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) domainUnmarshal.unmarshal(new File("server/src/main/files/domainJaxb.xml"));
        load(domain);
    }

    @Override
    public void domainSaveJacksonXML() throws IOException {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try(@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domainJackson.xml")) {
            @NotNull final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
            @NotNull final String domainJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if (domainJackson == null || domainJackson.trim().isEmpty()) throw new NullPointerException("Something is wrong");
            domainOut.write(domainJackson.getBytes());
            domainOut.flush();
        }
    }

    @Override
    public void domainLoadJacksonXML() throws IOException {
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        @Nullable final Domain domain = xmlMapper.readerFor(Domain.class).readValue(new File("server/src/main/files/domainJackson.xml"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }

    @Override
    public void domainSaveJacksonJSON() throws IOException {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try(@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domainJackson.json")) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String domainJackson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            domainOut.write(domainJackson.getBytes());
            domainOut.flush();
        }
    }

    @Override
    public void domainLoadJacksonJSON() throws IOException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readerFor(Domain.class).readValue(new File("server/src/main/files/domainJackson.json"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }
}
