package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {
    @WebMethod
    void domainSerializable(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainDeserializable(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainSaveJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainLoadJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainSaveJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainLoadJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainSaveJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainLoadJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainSaveJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void domainLoadJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;
}
