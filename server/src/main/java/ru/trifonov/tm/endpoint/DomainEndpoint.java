package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {
    public DomainEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void domainSerializable(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSerializable();
    }

    @Override
    @WebMethod
    public void domainDeserializable(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainDeserializable();
    }

    @Override
    @WebMethod
    public void domainSaveJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSaveJaxbXML();
    }

    @Override
    @WebMethod
    public void domainLoadJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainLoadJaxbXML();
    }

    @Override
    @WebMethod
    public void domainSaveJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSaveJaxbJSON();
    }

    @Override
    @WebMethod
    public void domainLoadJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainLoadJaxbJSON();
    }

    @Override
    @WebMethod
    public void domainSaveJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSaveJacksonXML();
    }

    @Override
    @WebMethod
    public void domainLoadJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSaveJacksonXML();
    }

    @Override
    @WebMethod
    public void domainSaveJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainSaveJacksonJSON();
    }

    @Override
    @WebMethod
    public void domainLoadJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getDomainService().domainLoadJacksonJSON();
    }
}
