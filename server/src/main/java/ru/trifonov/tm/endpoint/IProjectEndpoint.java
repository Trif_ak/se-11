package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void persistProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    ) throws Exception;

    @WebMethod
    void insertProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    void updateProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    Project getProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    List<Project> getAllProjectOfUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void deleteProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    void deleteAllProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    List<Project> sortByProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull String comparatorName
    ) throws Exception;

    @WebMethod
    List<Project> getProjectByPartString(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "partString", partName = "partString") @NotNull String partString
    ) throws Exception;

    @WebMethod
    List<Project> getAllProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;
}
