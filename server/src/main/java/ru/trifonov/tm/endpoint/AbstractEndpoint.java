package ru.trifonov.tm.endpoint;

import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractEndpoint {
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    List<RoleType> roles = Arrays.asList(RoleType.REGULAR_USER, RoleType.ADMIN);
}
