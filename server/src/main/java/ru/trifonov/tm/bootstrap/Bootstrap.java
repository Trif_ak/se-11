package ru.trifonov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.*;
import ru.trifonov.tm.api.service.*;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.repository.*;
import ru.trifonov.tm.service.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository, this);
    @NotNull private final IDomainService domainService = new DomainService(this);
    @NotNull private final PropertyService propertyService = new PropertyService();
    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    public void start() {
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init() throws IOException {
        initProperty();
        initEndpoint();
        initUsers();
    }

    private void initUsers() {
        userService.addUser();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void initProperty() throws IOException {
        propertyService.init();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        publisher(wsdl, endpoint);
    }

    private void publisher(@NotNull final String wsdl, @NotNull final Object endpoint) {
        Endpoint.publish(wsdl, endpoint);
    }
}