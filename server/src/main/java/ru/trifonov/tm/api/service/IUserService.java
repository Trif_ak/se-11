package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserService {
    void persist(@Nullable User user);
    User existsUser(@Nullable String login, @Nullable String password) throws NullPointerException;
    void registrationUser(@Nullable String login, @Nullable String password) throws NullPointerException;
    void registrationAdmin(@Nullable String login, @Nullable String password) throws NullPointerException;
    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws NullPointerException;
    List<User> getAll();
    User get(@Nullable String id) throws NullPointerException;
    void delete(@Nullable String id) throws NullPointerException;
    void deleteAll();
    void load(@Nullable List<User> users);
    void addUser();
    void changePassword(@Nullable String userId, @Nullable String newPassword);
}
