package ru.trifonov.tm.api;

import ru.trifonov.tm.api.service.*;

public interface ServiceLocator {
    IProjectService getProjectService();
    ITaskService getTaskService();
    IUserService getUserService();
    ISessionService getSessionService();
    IDomainService getDomainService();
    IPropertyService getPropertyService();
}
