package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.domain.Domain;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IDomainService {
    void export (@Nullable Domain domain);
    void load(@Nullable Domain domain);
    void domainSerializable() throws IOException;
    void domainDeserializable() throws IOException, ClassNotFoundException;
    void domainSaveJaxbXML() throws JAXBException;
    void domainLoadJaxbXML() throws JAXBException;
    void domainSaveJaxbJSON() throws JAXBException;
    void domainLoadJaxbJSON() throws JAXBException;
    void domainSaveJacksonXML() throws IOException;
    void domainLoadJacksonXML() throws IOException;
    void domainSaveJacksonJSON() throws IOException;
    void domainLoadJacksonJSON() throws IOException;
}
