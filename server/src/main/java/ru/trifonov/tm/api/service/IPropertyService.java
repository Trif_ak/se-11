package ru.trifonov.tm.api.service;

import java.io.IOException;

public interface IPropertyService {
    void init() throws IOException;
    String getServerHost();
    String getServerPort();
    String getServerSalt();
    Integer getServerCycle();
}
