package ru.trifonov.tm.api.service;

import ru.trifonov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task);
    void init(@Nullable String name, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate);
    void update(@Nullable String name, @Nullable String id, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws ParseException;
    Task get(@Nullable String id, @Nullable String userId);
    List<Task> getAllOfProject(@Nullable String projectId, @Nullable String userId);
    void delete(@Nullable String id, @Nullable String userId);
    void deleteAllOfProject(@Nullable String projectId, @Nullable String userId);
    void deleteAllOfUser(@Nullable String userId);
    List<Task> sortBy(@Nullable String projectId, @Nullable String userId, @Nullable String comparatorName);
    List<Task> getByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString);
    List<Task> getAll();
    void load(@Nullable List<Task> tasks);
}
