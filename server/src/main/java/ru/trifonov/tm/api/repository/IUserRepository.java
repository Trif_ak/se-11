package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);
    void merge(@NotNull User user);
    void insert(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);
    void update(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);
    User get(@NotNull String id) throws NullPointerException;
    List<User> getAll() throws NullPointerException;
    User existsUser(@NotNull String login, @NotNull String passwordMD5);
    Boolean getByLogin(@NotNull String login);
    void delete(@NotNull String id);
    void deleteAll();
    void changePassword(@NotNull String userId, @NotNull String newPasswordMD5);
}
