package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    void merge(@NotNull Project project);
    void persist(@NotNull Project project);
    void insert(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    void update(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    Project get(@NotNull String id, @NotNull String userId);
    List<Project> getAll(@NotNull String userId);
    void delete(@NotNull String id, @NotNull String userId);
    void deleteAll(@NotNull String userId);
    List<Project> getByPartString(@NotNull String userId, @NotNull String partString);
    List<Project> getAll();
}
