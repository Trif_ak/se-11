package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.Nullable;

import ru.trifonov.tm.entity.Project;


import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface IProjectService {
    void persist(@Nullable Project project) throws NullPointerException;
    void insert(@Nullable String name, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws ParseException;
    void update(@Nullable String name, @Nullable String id, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws ParseException;
    Project get(@Nullable String id, @Nullable String userId);
    List<Project> getAllOfUser(@Nullable String userId);
    void delete(@Nullable String id, @Nullable String userId);
    void deleteAll(@Nullable String userId) throws Exception;
    List<Project> sortBy(@Nullable String userId, @Nullable String comparatorName);
    List<Project> getByPartString(@Nullable String userId, @Nullable String partString);
    List<Project> getAll();
    void load(@Nullable List<Project> projects);
}
