package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    void persist(@NotNull Session session);
    void delete(@NotNull Session session);
    List<Session> getByUserId(@NotNull String userId);
    List<Session> getAll();
}
