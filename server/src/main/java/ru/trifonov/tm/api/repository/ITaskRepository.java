package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Task;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ITaskRepository {
    void merge(@NotNull Task task);
    void persist(@NotNull Task task);
    void insert(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    void update(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    List<Task> getAll(@NotNull String projectId, @NotNull String userId) throws NullPointerException;
    Task get(@NotNull String id, @NotNull String userId) throws NullPointerException;
    void delete(@NotNull String id, @NotNull String userId);
    void deleteAllOfProject(@NotNull String projectId, @NotNull String userId);
    void deleteAllOfUser(@NotNull String userId);
    List<Task> getByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString) throws NullPointerException;
    List<Task> getAll();
}
