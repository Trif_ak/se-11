package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Override
    public void persist(@NotNull final Session session) {
        entities.put(session.getId(), session);
    }

    @Override
    public void delete(@NotNull Session session) {
        entities.remove(session.getId());
    }

    @Override
    public List<Session> getByUserId(@NotNull final String userId) {
        @Nullable final List<Session> sessions = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Session> sessionEntry : entities.entrySet()) {
            if (sessionEntry.getValue().getUserId().equals(userId)){
                sessions.add(sessionEntry.getValue());
            }
        }
        if (sessions.isEmpty())  throw new NullPointerException("Not found sessions. Enter correct date");
        return sessions;
    }

    @Override
    public List<Session> getAll() {
        return new ArrayList<>(entities.values());
    }
}