package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Override
    public void persist(@NotNull final User user) {
        entities.put(user.getId(), user);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (entities.containsKey(user.getId())) {
            update(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        } else {
            insert(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        }
    }

    @Override
    public void insert(
            @NotNull final String id, @NotNull final String login,
            @NotNull final String passwordMD5, @NotNull final RoleType roleType
    ) {
        @NotNull final User user = new User(id, login, passwordMD5, roleType);
        entities.put(id, user);
    }

    @Override
    public void update(
            @NotNull final String id, @NotNull final String login,
            @NotNull final String passwordMD5, @NotNull final RoleType roleType
    ) {
        @NotNull final User user = new User(id, login, passwordMD5, roleType);
        entities.put(id, user);
    }

    @Override
    public User get(@NotNull final String id) throws NullPointerException {
        @NotNull final User user = entities.get(id);
        if (user == null) throw new NullPointerException("Not found user. Enter correct data");
        return user;
    }

    @Override
    public List<User> getAll() throws NullPointerException {
        @NotNull final List<User> output = new ArrayList<>(entities.values());
        if (output.isEmpty()) throw new NullPointerException("Not found users. Enter correct data");
        return output;
    }

    @Override
    public User existsUser(
            @NotNull final String login, @NotNull final String passwordMD5
    ) {
        @Nullable User findUser = null;
        for (@NotNull final User user : entities.values()) {
            if (user.getLogin().equals(login)) findUser = user;
        }
        if (findUser == null) return null;
        if (findUser.getPasswordMD5().equals(passwordMD5)) return findUser;
        return null;
    }

    @Override
    public Boolean getByLogin(@NotNull final String login) {
        for (@NotNull final User user : entities.values()) {
            if (user.getLogin().equals(login)) return true;
        }
        return true;
    }

    @Override
    public void delete(@NotNull final String id) {
        @NotNull final Iterator<Map.Entry<String, User>> entryIterator= entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            @NotNull final Map.Entry<String, User> userEntry = entryIterator.next();
            if (userEntry.getKey().equals(id)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void deleteAll() {
        entities.clear();
    }

    @Override
    public void changePassword(
            @NotNull final String userId, @NotNull final String newPasswordMD5
    ) {
        @NotNull User user = entities.get(userId);
        user.setPasswordMD5(newPasswordMD5);
    }
}

